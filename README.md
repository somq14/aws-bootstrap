# aws-bootstrap

## これはなに？

Terraform を使用して AWS アカウントをセットアップするリポジトリ  
Terraform の練習を兼ねているので勉強したことをメモしたりもしています

## できること

| ディレクトリ | 説明                                                                                                                |
| ------------ | ------------------------------------------------------------------------------------------------------------------- |
| s3-backend   | Amazon S3 を Terraform のバックエンドにするために必要なリソースを作成する<br>Terraform を使う準備のための Terraform |

## 前提条件

`terraform` コマンドがインストールされていること  
~~バージョン管理ツール [tfenv](https://github.com/tfutils/tfenv) を使うとよいです~~  
実行ファイルをダウンロードしてパスを通せば大丈夫

※ Windows 環境での tfenv だと VSCode の Terraform 拡張が terraform コマンドをみつけられないらしく、コード整形がうまくいきませんでした

AWS アクセスために API キーなどが設定されていること  
`AWS_PROFILE`環境変数を設定するのが楽だと思います  
他の手段はこのあたりをみるとわかる  
https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication-and-configuration

## Terraform コマンド集

ソースがあるディレクトリに移動してから実行するのが基本  
そこにあるすべての`*.tf`ファイルが勝手に読み込まれる

```bash
# Terraform やプロバイダーのバージョンを表示
# インストールできているかの確認とか
terraform version

# Terraform の作業ディレクトリを初期化する
# 初期ファイルを作成したり、プラグイン等をダウンロードしたりする
# 作業を始める前にマシンごとに実行しないといけない
# その点、git init とはニュアンスが違う
terraform init

# Terraform のファイルを整形する
terraform fmt

# Terraform のファイルを検証する
terraform validate

# プロビジョニングを計画し、リソースの差分を表示
terraform plan

# プロビジョニングを実行
terraform apply

# ステートを表示
# つまりクラウドに作成されているリソースたちが表示される
terraform show

# すべてのリソースを削除
terraform destroy

# コードで定義した出力値を表示する
terraform output
```

### Terraform 用語集

| 用語                | 説明                                                                                                                                                          |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| プロバイダー        | Terraform のプラグイン。これにより Terraform はいろいろなクラウドのリソースを管理できる。<br>例えば AWS のリソースを管理にするには AWS のプロバイダーを使う。 |
| ステート            | 作成されたリソースやその ID などのクラウドの状態を表すデータ。<br>Terraform のコードが TO-BE だとしたら、ステートは AS-IS。<br>tfstate ファイルに保存される。 |
| terraform.tfstate   | ステートが記載されたファイル。チーム開発では共有が必要。<br>センシティブな情報を含むため、Git にチェックインしてはいけない。                                  |
| バックエンド        | tfstate ファイルの保存先。AWS S3 など。                                                                                                                       |
| リソース            | Terraform で作成するクラウド上のモノ。Amazon EC2 インスタンスとか。                                                                                           |
| .terraform.lock.hcl | Terraform が依存するモジュールやプロバイダーのバージョンを固定するファイル。                                                                                  |
