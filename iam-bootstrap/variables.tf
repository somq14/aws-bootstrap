variable "region" {
  description = "AWS リージョン"
  type        = string
  default     = "ap-northeast-1"
}
