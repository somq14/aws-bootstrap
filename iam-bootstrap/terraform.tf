terraform {
  required_version = "~> 1.8.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.52.0"
    }
  }
  backend "s3" {
    # TODO: 適宜書き換えてください
    region         = "ap-northeast-1"
    bucket         = "tfstate-744532146409-ap-northeast-1"
    key            = "iam-bootstrap/terraform.tfstate"
    dynamodb_table = "tflock-744532146409-ap-northeast-1"
  }
}
