terraform {
  # 使用すべき Terraform のバージョンを指定する
  # バージョンはセマンティックバージョニング
  # ~> 1.8.4 は 1.8.4 以上の 1.8.x バージョンを指定する記法
  required_version = "~> 1.8.4"

  # プロバイダーを設定する
  # プロバイダーは Terraform のプラグインであり、Terraform はプロバイダーを使用して特定のクラウドを操作する
  required_providers {
    aws = {
      # ここでは AWS のプロバイダーを使用する
      # https://registry.terraform.io/providers/hashicorp/aws/5.52.0
      source = "hashicorp/aws"
      # プロバイダーのバージョンを指定する
      version = "~> 5.52.0"
    }
  }
}

# variable ブロックでは入力変数を定義できる
# Terraform の実行時に入力変数の値を設定できる
variable "region" {
  description = "AWS リージョン"
  type        = string
  default     = "ap-northeast-1"
}

# AWS のプロバイダーに対する設定を行う
provider "aws" {
  region = var.region
}

# data ブロックでデータソースを定義できる
# データソースは Terraform 外の情報を参照するための特別なリソース
# ここでは AWS アカウント ID の取得のためにデータソースを定義している
data "aws_caller_identity" "current" {}

# locals ブロックではローカル変数を宣言できる
# ローカル変数に長い式や複数回使用する式を割り当てておけばコードをシンプルにできる
locals {
  account_id = data.aws_caller_identity.current.account_id
}

# resource ブロックで Terraform で作成するリソースを定義する
# まずプロバイダーの提供するリソースタイプを、その次にリソース名を指定する
# リソースタイプとリソース名のセットがそのリソースの識別子となる
# Terraform のステートを管理する S3 バケット
resource "aws_s3_bucket" "tfstate" {
  bucket = "tfstate-${local.account_id}-${var.region}"
}
resource "aws_s3_bucket_versioning" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_lifecycle_configuration" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  rule {
    id     = "expiration-rule"
    status = "Enabled"
    # 90 日以上経過した 11 世代前以降のバージョンを削除する
    noncurrent_version_expiration {
      newer_noncurrent_versions = 10
      noncurrent_days           = 90
    }
  }
}

# Terraform がロックに使用する DynamoDB テーブル
resource "aws_dynamodb_table" "lock" {
  name     = "tflock-${local.account_id}-${var.region}"
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  billing_mode = "PAY_PER_REQUEST"
}

# output ブロックで出力値を定義できる
# これにより Terraform が管理する情報をコンソールに表示できる
output "aws_s3_bucketr_tfstate_id" {
  value = aws_s3_bucket.tfstate.id
}
output "aws_dynamodb_table_locK_name" {
  value = aws_dynamodb_table.lock.name
}
